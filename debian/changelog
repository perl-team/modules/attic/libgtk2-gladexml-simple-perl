libgtk2-gladexml-simple-perl (0.32-3) unstable; urgency=medium

  * Team upload

  [ Ansgar Burchardt ]
  * Update my email address.
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Florian Schlichting ]
  * Update d/copyright to copyright-format 1.0
  * Update license paragraphs to commonly used versions
  * Bump dh compat to level 11
  * Declare compliance with Debian Policy 4.1.4
  * Mark package autopkgtest-able

 -- Florian Schlichting <fsfs@debian.org>  Sat, 30 Jun 2018 18:00:55 +0200

libgtk2-gladexml-simple-perl (0.32-2) unstable; urgency=low

  [ gregor herrmann ]
  * Take over for the Debian Perl Group with maintainer's permission
    (http://lists.debian.org/debian-perl/2008/06/msg00039.html)
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Changed:
    Maintainer set to Debian Perl Group <pkg-perl-
    maintainers@lists.alioth.debian.org> (was: Florian Ragwitz
    <rafl@debian.org>); Florian Ragwitz <rafl@debian.org> moved to
    Uploaders.
  * Add debian/watch.

  [ Ryan Niebur ]
  * Remove Florian Ragwitz from Uploaders
  * Close ITA (Closes: #523135)

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * Refresh debian/rules for debhelper 7.
  * Convert debian/copyright to proposed machine-readable format.
  * Use source format 3.0 (quilt).
  * Bump Standards-Version to 3.8.4.
  * Add myself to Uploaders.

  [ gregor herrmann ]
  * Create manpage for gpsketcher with help2man.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sun, 14 Feb 2010 17:49:49 +0900

libgtk2-gladexml-simple-perl (0.32-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * debian/control: Recommend libxml-sax-perl (needed by gpsketcher, which is
    not always used) (Closes: #439797)
  * debian/rules: Don't fail when perl is smart enough not to create empty
    dirs (Closes: #467944)

 -- Marc 'HE' Brockschmidt <he@debian.org>  Sat, 05 Apr 2008 18:43:27 +0200

libgtk2-gladexml-simple-perl (0.32-1) unstable; urgency=low

  * Initial Release.

 -- Florian Ragwitz <rafl@debian.org>  Sun, 22 Jul 2007 14:38:11 +0200
